# Project motivation

The only purpose of this project is to verify some points about the
"out-of-order" job execution in Gitlab-CI pipelines, because we have
encountered some bad surprise with this new feature on OTB project.
